class CreatePostHasTags < ActiveRecord::Migration[5.1]
  def change
    create_table :post_has_tags do |t|
      t.references :tags
      t.references :posts
      t.timestamps
    end
  end
end
