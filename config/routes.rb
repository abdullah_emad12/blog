Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get "" => "users#index"
  get "register" => "users#registerForm"
  get "posts/add" => "posts#add"
  get "posts/view" => "posts#viewAll"
  get "posts/view/:id" => "posts#viewPost"
  get "/posts/edit/:id" => "posts#editPost"
  post "posts/add" => "posts#addPost"
  post "register" => "users#register"
  post "" => "users#login" 
  post "posts/delete/:id" => "posts#delete"
  post "comments/add/:id" => "posts#addComment"
  get "/comment/delete/:id" => "posts#deleteComment"
  post "/logout" => "users#logout"
  post "posts/edit/:id" => "posts#edit"

  namespace 'api' do 
  	resources :posts
  	post "users/login" => "users#login"
  end
end

