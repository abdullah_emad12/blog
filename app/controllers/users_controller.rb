require 'digest/sha1'
class UsersController < ApplicationController
 	skip_before_action :verify_authenticity_token
  def index 
  	render :action => "index"
  end

  	def registerForm
  		render :action => "register"
  	end

  	def register
  		if(!params["password"].eql? params["password2"])
  			@email = params["email"]
  			@name = params["name"]
  			@error = "Passwords do not match"
  		else
  			encrypted_password= Digest::SHA1.hexdigest(params["password"])
  			begin
  			user = User.create(name: params["name"], email: params["email"], password: encrypted_password)
  			rescue
		    	@error = "email already taken"
		    end
		    if(!@error)
  				session[:user_id] = user.id
  				redirect_to "/posts/add"
  			end
  		end
  	end

  	def login
      encrypted_password= Digest::SHA1.hexdigest(params["password"])
    	users = User.where(email: params["email"])
      if(users.any?)
          user = User.find(users.ids[0])
          if(!user.password.eql?encrypted_password)
          @error = "Incorrect username or password"
          render :action => "index"
          else
            session[:user_id] = user.id
            redirect_to "/posts/add"
          end
          
      else
        @error = "Incorrect username or password"
        render :action => "index"

      end
  	end

   def logout
    session.delete(:user_id)
    redirect_to("/")

   end
end