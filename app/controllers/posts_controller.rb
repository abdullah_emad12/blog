class PostsController < ApplicationController
 skip_before_action :verify_authenticity_token
def add
	if(!session[:user_id])
		redirect_to("/")
	else
		render :action => "add"
	end
end

def addPost
	if(!session[:user_id])
		redirect_to("/")
	else
		tags = params["tags"].split(" ")
		post = Post.create(title: params["title"], content: params["content"], users_id: session[:user_id])
		tags.each { |x|  
			if Tag.where(name: x).empty?
				tag = Tag.create(name: x) 
				PostHasTag.create(tags_id: tag.id, posts_id: post.id)
			else
				tag = Tag.where(name: x)
				PostHasTag.create(tags_id: tag.ids[0], posts_id: post.id)
			end
		}
		redirect_to("/posts/view")

		
	end
end

def viewAll
	@posts = Post.all()
	render :action =>"index"
end

 def viewPost
 		begin
 			@post = Post.find(params[:id])
 			@comments = Comment.where(posts_id: params[:id])
 			@users = Array.new
 			@comments.each { |x| 
 			 	@users.push User.find(x.users_id)
 			}

		rescue
			render :action => "error"
			return
   		end
   		@user = User.find(@post.users_id)
        render :action => "view"
        
 end

def delete
	post = Post.find(params[:id])
   	if(post.users_id == session[:user_id])
   		post.destroy()
	 	redirect_to("/posts/view");
	 else
	 	redirect_to("/");
	 end

end

def addComment
	if(!session[:user_id])
		redirect_to("/")
	else
		Comment.create(content: params[:comment], users_id: session[:user_id], posts_id: params[:id])
		redirect_to("/posts/view/" + params[:id])
	end

end

def deleteComment
	if(!session[:user_id])
		redirect_to("/")
	else
		comment = Comment.find(params[:id])
		if(comment.users_id != session[:user_id])
			render :action => "error"
		else
			pid = comment.posts_id
			comment.destroy()
			redirect_to("/posts/view/" + comment.posts_id.to_s)

		end

	end

end

def editPost
	if(!session[:user_id])
		redirect_to("/")
	else
		@post = Post.find(params[:id])
		@tagsid = PostHasTag.where(posts_id: @post.id)
		@tags = Array.new
		@tagsid.each { |x| 
 			@tags.push Tag.find(x.tags_id)
 		}

		render :action => "edit"

	end

end
def edit
	@post = Post.find(params[:id])
	if(@post.users_id != session[:user_id])
		redirect_to("/")
	else
		@post.update(title: params[:title], content: params[:content])

		old_tags = PostHasTag.where(posts_id: @post.id)
		old_tags.each{ |x|
			x.destroy()
		 }

		tags = params["tags"].split(" ")
		tags.each { |x|  
			if Tag.where(name: x).empty?
				tag = Tag.create(name: x) 
				PostHasTag.create(tags_id: tag.id, posts_id: @post.id)
			else
				tag = Tag.where(name: x)
				tag.each { |y|

				if(PostHasTag.where(tags_id: y.id).empty?)
					PostHasTag.create(tags_id: y.id, posts_id: @post.id)
				end
			}
			end

		}
		redirect_to("/posts/view")

	end

end

end