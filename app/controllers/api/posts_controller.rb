module Api 
		class PostsController < ApplicationController
		skip_before_action :verify_authenticity_token
			def index
				posts = Post.order('created_at DESC')
				render json: {
					status: "OK",
					count: posts.count,
					data: posts

				}
			end

			def create 
				if(!session[:user_id])
					render json: {
					status: "Unauthorized",
					code: 403
				}
				else		
					post = Post.new(users_id: session[:user_id], title: params["title"], content: params[:content])
					post.save()
					post.save
					render json: {
						status: "Ok",
						code: 200
					}
				end

			end

			private 
			def post_params
				params.permit(:title, :content)
			end
		end 
end