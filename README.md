# Blog using ruby on rails

## Features in this project 

1. login/logout
2. Email registeration
3. Create posts
3. Read posts
4. Update posts
5. Delete Posts
6. Comment on posts
7. Delete your comments.
8. API for login Example: send a post request with the following parameters Email: example@mail.com	password: secret
9. API for retreiving all posts: GET request to 127.0.0.1:3000//api/posts 
10. API for creating new posts Example: send a post request with the following parameters title: Title	Content: body 
Note that in order for this to work the user must be logged in first. either with the API or from the website


## Start the website 

run the following in the directory where the project was cloned
`rails server`

then type in the browser's address bar: 

`127.0.0.1:3000`

